<?php

use Dotenv\Dotenv;

class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $dotenv = new Dotenv(__DIR__ . '/../', '.env.testing');
        $dotenv->load();

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    public function initToken($givenUser = 1)
    {
        $sandboxApiUrl = env('SANDBOX_API_TOKEN');

        if ( $givenUser === 1 ) {
            $params = array('email' => 'marius@amazing.com', 'password' => '128064');
        }

        if ( $givenUser === 2 ) {
            $params = array('email' => 'chris@amazing.com', 'password' => 'Amazing12341!');
        }

        if ( $givenUser === 3 ) {
            $params = array('email' => 'gary@amazing.com', 'password' => '128064');
        }

        // Build Http query using params
        $query = http_build_query ($params);

        // Create Http context details
        $contextData = array (
            'method' => 'POST',
            'header' =>
                "Content-Type: application/x-www-form-urlencoded\r\n" .
                "Connection: close\r\n" .
                "Content-Length: ".strlen($query)."\r\n",
            'content'=> $query );

        // Create context resource for our request
        $context = stream_context_create (array ( 'http' => $contextData ));

        // Read page rendered as result of your POST request
        $result =  file_get_contents (
            $sandboxApiUrl,
            false,
            $context);

        return $result;
    }
}
