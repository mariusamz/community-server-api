<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Tymon\JWTAuth\JWTManager as JWT;


class SearchTest extends TestCase
{

    public function setUp()
    {
        parent::setUp();
        Artisan::call('migrate:refresh');
    }

    /**
     * A basic search functionalitiy test
     *
     * @return void
     */
    public function testBasicSearch()
    {
        $tokenRes = $this->initToken();
        $tokenRes = json_decode($tokenRes);

        //Question creation call
        $response = $this->call('POST', '/api/questions', array(
            'text' => 'this is the question #tagAgain1 #tagAgain2',
            'description' => 'this is the description'
        ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());

        $responseArr['content'] = (array)$responseArr['content'];
        $questionId = $responseArr['content']['id'];
        $this->assertEquals(201, $response->getStatusCode());


        //Question creation call
        $response = $this->call('POST', '/api/questions', array(
            'text' => 'this is the question2 #tag1 #tag2',
            'description' => 'this is the description2'
        ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());

        $responseArr['content'] = (array)$responseArr['content'];
        $questionId2 = $responseArr['content']['id'];
        $this->assertEquals(201, $response->getStatusCode());


        //verify search call for ASM7 audience returns 0 entries
        $response = $this->call('GET', '/api/search/audience/ASM7', [
            'text' => 'the question',
        ],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());

        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue(empty($responseArr['content']));
        $this->assertEquals(200, $response->getStatusCode());

        //Question creation call no3 audience ASM7
        $response = $this->call('POST', '/api/questions/audience/ASM7', array(
            'text' => 'this is the question1 audience ASM7 #tagAgain1 #tagAgain2',
            'description' => 'this is the description ASM7'
        ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());

        $responseArr['content'] = (array)$responseArr['content'];
        $questionIdAsm = $responseArr['content']['id'];
        $this->assertEquals(201, $response->getStatusCode());

        //create answer for asm
        $response = $this->call('POST', '/api/questions/' . $questionIdAsm . '/answers'
            , array(
                'text' => 'this is the first answer codewise for ASM7'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );


        //verify actual search call
        $response = $this->call('GET', '/api/search', [
                'text' => 'the question',
            ],
                [/* cookies */],
                [/* files */],
                ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());

        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue(isset($responseArr['content'][0]->id));
        $this->assertTrue(isset($responseArr['content'][0]->numberOfAnswers));
        $this->assertTrue(isset($responseArr['content'][0]->text));
        $this->assertTrue(isset($responseArr['content'][0]->detail));
        $this->assertTrue(isset($responseArr['content'][0]->user));
        $this->assertTrue(isset($responseArr['content'][0]->hasAcceptedAnswer));
        $this->assertTrue(isset($responseArr['content'][0]->numberOfAnswers));
        $this->assertTrue(isset($responseArr['content'][0]->tags));
        $this->assertTrue(isset($responseArr['content'][0]->created_at));
        $this->assertTrue(isset($responseArr['content'][0]->updated_at));

        $this->assertEquals(200, $response->getStatusCode());

        //verify actual search call for ASM7 audience returns 1 entry
        $response = $this->call('GET', '/api/search/audience/ASM7', [
                'text' => 'the question',
            ],
                [/* cookies */],
                [/* files */],
                ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());

        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue(isset($responseArr['content'][0]->id));
        $this->assertTrue(isset($responseArr['content'][0]->numberOfAnswers));
        $this->assertTrue($responseArr['content'][0]->text == 'this is the question1 audience ASM7 #tagAgain1 #tagAgain2');
        $this->assertTrue(isset($responseArr['content'][0]->detail));
        $this->assertTrue(isset($responseArr['content'][0]->user));
        $this->assertTrue(isset($responseArr['content'][0]->hasAcceptedAnswer));
        $this->assertTrue(isset($responseArr['content'][0]->numberOfAnswers));
        $this->assertTrue(isset($responseArr['content'][0]->tags));
        $this->assertTrue(isset($responseArr['content'][0]->created_at));
        $this->assertTrue(isset($responseArr['content'][0]->updated_at));

        $this->assertEquals(200, $response->getStatusCode());


        //adding the first answer
        $response = $this->call('POST', '/api/questions/' . $questionId2 . '/answers'
            , array(
                'text' => 'this is the first answer codewise'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );


        //verify actual search, hit content from the answer and ignore ASM7 content
        $response = $this->call('GET', '/api/search', [
            'text' => 'codewise',
        ],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());

        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue(count($responseArr['content']) === 1);
        $this->assertTrue(isset($responseArr['content'][0]->id));
        $this->assertTrue(isset($responseArr['content'][0]->numberOfAnswers));
        $this->assertTrue($responseArr['content'][0]->text === 'this is the question2 #tag1 #tag2');
        $this->assertTrue(count($responseArr['content'][0]->tags) === 2);
        $this->assertTrue($responseArr['content'][0]->tags[0]->name === 'tag1');
        $this->assertTrue($responseArr['content'][0]->tags[1]->name === 'tag2');
        $this->assertEquals(200, $response->getStatusCode());


        //verify actual search, hit content from the answer of ASM7 content and ignore campus
        $response = $this->call('GET', '/api/search/audience/ASM7', [
            'text' => 'codewise',
        ],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());

        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue(count($responseArr['content']) === 1);
        $this->assertTrue(isset($responseArr['content'][0]->id));
        $this->assertTrue(isset($responseArr['content'][0]->numberOfAnswers));
        $this->assertTrue($responseArr['content'][0]->text === 'this is the question1 audience ASM7 #tagAgain1 #tagAgain2');
        $this->assertTrue(count($responseArr['content'][0]->tags) === 2);
        $this->assertTrue($responseArr['content'][0]->tags[0]->name === 'tagagain1');
        $this->assertTrue($responseArr['content'][0]->tags[1]->name === 'tagagain2');
        $this->assertEquals(200, $response->getStatusCode());

    }
}