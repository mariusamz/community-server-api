<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class TagsTest extends TestCase
{

    public function setUp()
    {
        parent::setUp();
        Artisan::call('migrate:refresh');
    }

    /**
     * A basic functional questions example.
     *
     * @return void
     */
    public function testAutoSuggestTags()
    {
        $tokenRes = $this->initToken();
        $tokenRes = json_decode($tokenRes);

        //creation of a question and corresponding tags
        $response = $this->call('POST', '/api/questions', array(
            'text' => 'this is the question',
            'description' => 'this is the description #babum #name #vrum',
            'user' => 'user',
        ),
        [/* cookies */],
        [/* files */],
        ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array) json_decode($response->getContent());
        $this->assertEquals(201, $response->getStatusCode());

        //list all autosugested tags, we should have none at this point
        $response = $this->call('GET', '/api/tags');

        $responseArr = (array) json_decode($response->getContent());
        $this->assertTrue(count($responseArr) === 0);
        $this->assertEquals(200, $response->getStatusCode());

        //set all three tags to be autosuggested
        DB::connection()->getPdo()->exec("UPDATE forum_tags SET is_auto_suggested = 1 Where 1");

        //list all autosugested tags
        $response = $this->call('GET', '/api/tags');

        $responseArr = (array) json_decode($response->getContent());
        $this->assertTrue(count($responseArr) === 3);
        $this->assertEquals(200, $response->getStatusCode());

        foreach($responseArr as $tag){
            //$this->assertTrue(property_exists($tag, 'id'));
            $this->assertTrue(property_exists($tag, 'name'));
        }

    }
}
