<?php

/**
 * Created by PhpStorm.
 * User: marius
 * Date: 08/03/2017
 * Time: 11:38
 */
class TokenTest extends TestCase
{

    public function setUp()
    {
        parent::setUp();
    }

    public function testAuthResponseWhenJWTExpired()
    {
        $expiredJwt = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvc2FuZGJveC5hbWF6aW5nYmV0YS5jb21cL3Jlc3RcL3YyXC9hdXRoXC9sb2dpbiIsImlhdCI6MTQ4NTg2Njg3OCwiZXhwIjoxNDg1ODY3NDc4LCJhdWQiOiJ3d3cuZXhhbXBsZS5jb20iLCJzdWIiOjE4NTA5NCwidXNlcm5hbWUiOiJtYXJpdXMiLCJuYmYiOjE0ODU4NjY4NzgsImp0aSI6ImMyN2FhNGJkZWQ3MzkzOWVlNGZlZDQyMTQ4YjMyMTk5IiwiQU1NX21zbGlkIjo0NywiUF9tc2xpZHMiOiIyMywzOCwyMiJ9.05u6uJ5foHkAjbhopKrDXceiUXI6LfV7H1KhwafCqtI';

        //creation of a question and corresponding tags
        $response = $this->call('POST', '/api/questions', array(
            'text' => 'this is the question',
            'description' => 'this is the description #babum #name #vrum',
            'user' => 'user',
        ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $expiredJwt]
        );

        $responseArr = (array) json_decode($response->getContent());
        $this->assertEquals(401, $response->getStatusCode());

        $this->assertTrue(isset($responseArr['error']) && $responseArr['error'] === 'token_expired');
        $this->assertTrue(isset($responseArr['auth']) && $responseArr['auth']->status === 'token_expired');
    }
}