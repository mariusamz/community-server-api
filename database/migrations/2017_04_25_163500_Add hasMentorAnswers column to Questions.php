<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHasMentorAnswersColumnToQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::connection()->getPdo()->exec("
                ALTER TABLE `forum_questions` 
        ADD COLUMN `has_mentor_answers` TINYINT(1) NULL DEFAULT 0 AFTER `number_of_answers`;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::connection()->getPdo()->exec("
                    ALTER TABLE `forum_questions` 
            DROP COLUMN `has_mentor_answers`;
         ");
    }
}
