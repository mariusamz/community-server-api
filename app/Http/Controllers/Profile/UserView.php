<?php
/**
 * Created by PhpStorm.
 * User: marius
 * Date: 01/09/2016
 * Time: 11:35
 */

namespace App\Http\Controllers\Profile;


class UserView
{
    public $id;
    public $username;
    public $avatarUrl;
    public $isMentor;

    /**
     * AnswersView constructor.
     * @param $userArr
     */
    public function __construct($userArr)
    {
        $this->id = $userArr['usid'];
        $this->username = $userArr['username'];
        $this->isMentor = $userArr['is_mentor'];

        if(!empty($userArr['avatar_url'])) {
            $this->avatarUrl = $userArr['avatar_url'];
        } else {
            $this->avatarUrl = '';
        }
    }
}