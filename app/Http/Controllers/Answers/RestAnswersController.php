<?php

namespace App\Http\Controllers\Answers;


use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Amazing\Model\Answer;
use Amazing\Model\Question;
use Amazing\Model\Tag;
use Amazing\Model\Member;
use Amazing\Model\AnswerVote;
use App\Http\Controllers\Tags;
use Illuminate\Support\Facades\Log;
use Amazing\Services\Auth\AuthService;
use Amazing\Services\QuestionEvents;
use Amazing\Services\NotificationService;


class RestAnswersController extends BaseController
{

    private $authService;
    private $questionEvents;

    /**
     * @var NotificationService
     */
    private $notificationService;

    /**
     * RestAnswersController constructor.
     * @param AuthService $authService
     * @param QuestionEvents $questionEvents
     * @param NotificationService $notificationService
     */
    function __construct(AuthService $authService, QuestionEvents $questionEvents, NotificationService $notificationService)
    {
        $this->authService = $authService;
        $this->questionEvents = $questionEvents;
        $this->notificationService = $notificationService;
    }

    /**
     * Get a single answer identified by answer id and question id
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function getAnswer(Request $request)
    {
        if (is_null($request->route('answer_id'))) {
            return response()->json('Validation error', 400);
        }

        if (is_null($request->route('question_id'))) {
            return response()->json('Validation error', 400);
        }

        $filterByAudience = '';
        if (!is_null($request->route('audience'))) {

            $response = $this->authService->validateRequestOrigin(
                $request->route('audience')
            );

            $filterByAudience = $request->route('audience');

            if($response !== true){
                return response()->json('No rights to see this content', 403);
            }
        }

        $question = Question::where('audience', $filterByAudience)
            ->where('id', $request->route('question_id'))
            ->first();

        if (empty($question)) {
            return response()->make('Question associated to answers not found', 404);
        }

        Log::debug("Request answer started " . $request->route('question_id') . ' -- ' . $request->route('answer_id'));

        $answer = Answer::with(['member', 'answerVotes' => function ($query) {
                    $query->where('user_id', Auth::user()->usid);
                }])
            ->where('id', $request->route('answer_id'))
            ->where('question_id', $request->route('question_id'))
            ->first();

        if (empty($answer)) {
            return response()->make('', 404);
        }

        $hasCurrentUserVoted = false;
        if (!empty($answer->answerVotes->toArray())) {
            $hasCurrentUserVoted = true;
        }

        $voteView = new VoteView(
            [
                'count' => $answer->votes,
                'hasCurrentUserVoted' => $hasCurrentUserVoted
            ]
        );

        $answerArr = $answer->toArray();
        $answerArr['user'] = $answer->member;
        $answerArr['voteView'] = $voteView;

        $answerView = new AnswersView(
            $answerArr
        );

        return response()->json([
                'content' => $answerView,
                'server_date' => strtotime('now')
            ]
        );
    }

    /**
     * Create an answer for a given question.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function createAnswer(Request $request)
    {
        if (is_null($request->get('text'))) {
            return response()->json('Validation error', 400);
        }

        if (is_null($request->route('question_id'))) {
            return response()->json('Validation error', 400);
        }

        Log::debug("Request answer creation started " . $request->route('question_id'));

        //get the details of the user after login
        /**
         * @var $member Member
         */
        $member = $this->authService->getAuthenticatedUser();
        if ($member == null) {
            return response()->json('Authentication error', 403);
        }

        $question = Question::where('id', $request->route('question_id'))->first();
        if (empty($question)) {
            return response()->make('Question does not exist', 400);
        }

        //if the owner of the answer is a mentor we update the question has_mentor_answers flag
        if ($member->is_mentor == 1){
            $question->has_mentor_answers = 1;
        }

        $text = htmlspecialchars($request->get('text'), ENT_NOQUOTES, 'UTF-8', false);

        preg_match_all("/#([a-zA-Z0-9\-]+)/", $text, $tagsRegArr);

        //Extract Tags out of the given text (and create the tags if they do not exists in db) +
        //attach them to the newly created answer
        $countNonUniqueTags = 0;
        $tagsArr = [];
        if (isset($tagsRegArr[1]) && !empty($tagsRegArr[1])) {
            if (is_array($tagsRegArr)) {
                $countNonUniqueTags = count($tagsRegArr[1]);
            }

            $tagsRegArr[1] = array_map('strtolower', $tagsRegArr[1]);
            $tagsArr = array_unique($tagsRegArr[1]);

            if ($countNonUniqueTags >= 20) {
                return response()->json('Validation error: maximum 19 tags permitted.', 400);
            }

            $tags = Tag::whereIn('name', $tagsArr)->get();
            $tagsArr = array_flip($tagsArr);
        }

        $answer = new Answer();
        $answer->text = $text;
        $answer->question()->associate($question);
        $answer->member()->associate($member);
        $answer->save();

        //update no of answers per question
        $question->number_of_answers = $question->number_of_answers + 1;
        $question->save();

        if (isset($tags)) {
            foreach ($tags as $tag) {
                if (isset($tagsArr[$tag->name])) {
                    $answer->tags()->save($tag);
                    unset($tagsArr[$tag->name]);
                }
            }

            $tagsToPersist = [];
            foreach ($tagsArr as $key => $tag) {
                $tag = new Tag();
                $tag->name = $key;
                $tag->save();

                $tagsToPersist[] = $tag;
            }

            if (!empty($tagsToPersist)) {
                $answer->tags()->saveMany($tagsToPersist);
            }
        }

        $this->questionEvents->updateActiveQuestionInfo($request->route('question_id'));

        //check for ASM7 availability
        $response = $this->authService->validateRequestOrigin(
            'ASM7'
        );

        if($response === true) {
            $this->notificationService->answerAddEvent($answer, $question);
        }

        return response()->make(
            ['content' => ['id' => $answer->id]],
            201
        );
    }


    /**
     * Edit an answer given by id.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function editAnswer(Request $request)
    {
        if (is_null($request->get('text'))) {
            return response()->json('Validation error', 400);
        }

        if (is_null($request->route('question_id'))) {
            return response()->json('Validation error', 400);
        }

        if (is_null($request->route('answer_id'))) {
            return response()->json('Validation error', 400);
        }

        $filterByAudience = '';
        if (!is_null($request->route('audience'))) {

            $response = $this->authService->validateRequestOrigin(
                $request->route('audience')
            );

            $filterByAudience = $request->route('audience');

            if($response !== true){
                return response()->json('No rights to see this content', 403);
            }
        }

        Log::debug("Request answer creation started " . $request->route('answer_id'));

        //Enforce question_id belongs to filter and question exists
        $question = Question::where('id', $request->route('question_id'))
            ->where('audience', $filterByAudience)
            ->first();

        if (empty($question)) {
            return response()->make('Not found question', 404);
        }


        //get the details of the user after login
        /**
         * @var $member Member
         */
        $member = $this->authService->getAuthenticatedUser();
        if ($member == null) {
            return response()->json('Authentication error', 403);
        }

        //check that the call user is a mentor
        if ($member->is_mentor !== 1) {
            return response()->json('Validation error, not a mentor', 400);
        }

        $answer = Answer::where('id', $request->route('answer_id'))
            ->where('question_id', $request->route('question_id'))
            ->first();

        $text = htmlspecialchars($request->get('text'), ENT_NOQUOTES, 'UTF-8', false);

        preg_match_all("/#([a-zA-Z0-9\-]+)/", $text, $tagsRegArr);

        //Extract Tags out of the given text (and create the tags if they do not exists in db) +
        //attach them to the newly created answer
        $countNonUniqueTags = 0;
        $tagsArr = [];
        if (isset($tagsRegArr[1]) && !empty($tagsRegArr[1])) {
            if (is_array($tagsRegArr)) {
                $countNonUniqueTags = count($tagsRegArr[1]);
            }

            $tagsRegArr[1] = array_map('strtolower', $tagsRegArr[1]);
            $tagsArr = array_unique($tagsRegArr[1]);

            if ($countNonUniqueTags >= 20) {
                return response()->json('Validation error: maximum 19 tags permitted.', 400);
            }

            $tags = Tag::whereIn('name', $tagsArr)->get();
            $tagsArr = array_flip($tagsArr);
        }

        $answer->text = $text;
        $answer->save();

        if (isset($tags)) {
            foreach ($tags as $tag) {
                if (isset($tagsArr[$tag->name])) {
                    $answer->tags()->save($tag);
                    unset($tagsArr[$tag->name]);
                }
            }

            $tagsToPersist = [];
            foreach ($tagsArr as $key => $tag) {
                $tag = new Tag();
                $tag->name = $key;
                $tag->save();

                $tagsToPersist[] = $tag;
            }

            if (!empty($tagsToPersist)) {
                $answer->tags()->saveMany($tagsToPersist);
            }
        }

        $this->questionEvents->updateActiveQuestionInfo($request->route('question_id'));

        return response()->make(
            '',
            200
        );
    }

    /**
     * Return all answers for a given question.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function getAnswers(Request $request)
    {
        if (is_null($request->route('question_id'))) {
            return response()->json('Validation error', 400);
        }

        $filterByAudience = '';
        if (!is_null($request->route('audience'))) {

            $response = $this->authService->validateRequestOrigin(
                $request->route('audience')
            );

            $filterByAudience = $request->route('audience');

            if($response !== true){
                return response()->json('No rights to see this content', 403);
            }
        }

        Log::debug("Request all answers started " . $request->route('question_id'));

        $question = Question::where('audience', $filterByAudience)
            ->where('id', $request->route('question_id'))
            ->first();

        if (empty($question)) {
            return response()->make('Question associated to answers not found', 404);
        }

        if (!is_null($request->get('mode')) && $request->get('mode') === 'acceptedAndMentors') {

            $answers = Answer::with(['member', 'answerVotes' => function ($query) {
                $query->where('user_id', Auth::user()->usid);
            }])
                ->leftjoin('members as m', 'm.usid', '=', 'forum_answers.user_id')
                ->where('question_id', $request->route('question_id'))
                ->where(function ($query){
                    $query->where('m.is_mentor', 1)
                          ->orWhere('forum_answers.accepted', 1);
                })
                ->orderBy('accepted', 'desc')
                ->orderBy('votes', 'desc')
                ->orderBy('created_at', 'desc')
                ->get();
        } else {
            $answers = Answer::with(['member', 'answerVotes' => function ($query) {
                $query->where('user_id', Auth::user()->usid);
            }])
                ->where('question_id', $request->route('question_id'))
                ->orderBy('accepted', 'desc')
                ->orderBy('votes', 'desc')
                ->orderBy('created_at', 'desc')
                ->get();
        }

        $response = [];
        foreach ($answers as $answer) {
            $answersArr = $answer->toArray();
            $answersArr['user'] = $answer->member;

            $hasCurrentUserVoted = false;
            if (!empty($answer->answerVotes->toArray())) {
                $hasCurrentUserVoted = true;
            }

            $voteView = new VoteView(
                [
                    'count' => $answer->votes,
                    'hasCurrentUserVoted' => $hasCurrentUserVoted
                ]
            );

            $answersArr['voteView'] = $voteView;

            $response[] = new AnswersView(
                $answersArr
            );
        }

        return response()->json(
            [
                'content' => $response,
                'server_date' => strtotime('now')
            ]
        );
    }

    /**
     * Create a vote to an answer.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function createAnswerVote(Request $request)
    {
        if (is_null($request->route('answer_id'))) {
            return response()->json('Validation error', 400);
        }

        if (is_null($request->get('state'))) {
            return response()->json('Validation error', 400);
        }

        Log::debug("Request vote answer creation started " . $request->route('answer_id'));

        $answer = Answer::where('id', $request->route('answer_id'))
            ->first();

        if (empty($answer)) {
            return response()->make('', 404);
        }

        $state = $request->get('state');

        $answerVote = AnswerVote::where('answer_id', $request->route('answer_id'))
            ->where('user_id', Auth::user()->usid)
            ->first();

        // in case of voting up
        if ($state === '+1') {
            if (!empty($answerVote)) {
                return response()->json('Duplicate voting', 422);
            } else {
                $answerVote = new AnswerVote();
                $answerVote->answer()->associate($answer);
                $answerVote->member()->associate(Auth::user());
                $answerVote->save();

                $answer->votes = $answer->votes + 1;
                $answer->save();

                //update active event for the question owning this answer
                $this->questionEvents->updateActiveQuestionInfo($request->route('question_id'));

                return response()->make('', 201);
            }
        }
        // in case of unvoting
        elseif ($state === '0') {
            if (empty($answerVote)) {
                return response()->json('Unvote is not allowed', 422);
            } else {
                $answerVote->delete();

                $answer->votes = $answer->votes - 1;
                $answer->save();

                //update active event for the question owning this answer
                $this->questionEvents->updateActiveQuestionInfo($request->route('question_id'));

                return response()->make('', 201);
            }
        }

        return response()->json('Something went wrong', 422);
    }


    /**
     * Create a solution to a question.
     *
     * Only the creator of the question can select an answer as the solution and only one answer can be selected.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function createSolution(Request $request)
    {
        if (is_null($request->route('question_id'))) {
            return response()->json('Validation error', 400);
        }

        if (is_null($request->route('answer_id'))) {
            return response()->json('Validation error', 400);
        }

        if (is_null($request->get('state'))) {
            return response()->json('Validation error', 400);
        }

        Log::debug("Request select solution creation started " . $request->route('question_id') . ' ' .
            $request->route('answer_id'));

        $question = Question::with(['answers' => function ($query) use ($request) {
            $query->where('id', $request->route('answer_id'))->where('accepted', 0)->first();
        }])
            ->where('id', $request->route('question_id'))
            ->where('user_id', Auth::user()->usid)
            ->where('has_accepted_answer', 0)
            ->first();

        if (empty($question)) {
            return response()->make('Question validation error', 422);
        }

        if (!isset($question->answers[0])) {
            return response()->make('Answer validation error', 422);
        }

        // in case of accepting an answer for question
        $state = $request->get('state');
        if ($state === '1') {
            $question->has_accepted_answer = 1;
            $question->answers[0]->accepted = 1;
            $question->answers[0]->save();
            $question->save();

            //update active event for the question owning this answer
            $this->questionEvents->updateActiveQuestionInfo($request->route('question_id'));

            return response()->make('', 201);
        }
        else {
            return response()->make('State is incorect', 422);
        }
    }
}
