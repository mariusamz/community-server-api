<?php
/**
 * Created by PhpStorm.
 * User: marius
 * Date: 01/09/2016
 * Time: 11:35
 */

namespace App\Http\Controllers\Questions;

use App\Http\Controllers\Tags\TagsView;
use App\Http\Controllers\Profile\UserView;

class QuestionsView
{
    public $id;
    public $text;
    public $detail;
    public $user;
    public $hasAcceptedAnswer;
    public $numberOfAnswers;
    public $hasMentorAnswers;
    public $tags = [];
    public $created_at;
    public $updated_at;

    /**
     * QuestionsView constructor.
     * @param $questionArr
     */
    public function __construct($questionArr)
    {
        $this->id = $questionArr['id'];
        $this->text = $questionArr['text'];
        $this->detail = $questionArr['detail'];
        $this->user = new UserView($questionArr['user']);
        $this->hasAcceptedAnswer = $questionArr['has_accepted_answer'];
        $this->numberOfAnswers = $questionArr['number_of_answers'];
        $this->hasMentorAnswers = $questionArr['has_mentor_answers'];

        if(isset($questionArr['currentUserCanAcceptSolution'])){
            $this->currentUserCanAcceptSolution = $questionArr['currentUserCanAcceptSolution'];
        }

        //lesson and course specific data
        if(isset($questionArr['cid'])){
            $this->courseId = $questionArr['cid'];
        }
        if(isset($questionArr['title'])){
            $this->lessonTitle = $questionArr['title'];
        }
        if(isset($questionArr['lesson_id'])){
            $this->lessonId = $questionArr['lesson_id'];
        }

        $this->created_at = strtotime($questionArr['created_at']);
        $this->updated_at = strtotime($questionArr['updated_at']);

        if(isset($questionArr['objTags'])) {
            foreach ($questionArr['objTags'] as $tag) {

                if(!empty($tag)) {
                    if (is_object($tag))
                        $tagArr = $tag->toArray();
                    else
                        $tagArr['name'] = $tag;

                    $this->tags[] = new TagsView($tagArr);
                }
            }
        }
    }

}