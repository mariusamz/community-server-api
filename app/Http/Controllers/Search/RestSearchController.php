<?php

namespace App\Http\Controllers\Search;

use App\Http\Controllers\Questions\QuestionsView;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Amazing\Model\Question;
use Amazing\Model\Member;
use Illuminate\Support\Facades\Log;
use Amazing\Services\Auth\AuthService;


class RestSearchController extends BaseController
{
    private $auth_service;

    function __construct(AuthService $auth_service)
    {
        $this->authService = $auth_service;
    }

    /**
     * Return all questions that contain the searched field
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function performSearch2(Request $request)
    {
        if (is_null($request->get('text'))) {
            return response()->json('Validation error', 400);
        }

        Log::debug("Request search started " . $request->get('text') . microtime());

        $questions = DB::select("
          Select * FROM (SELECT fq.*, mb.usid, mb.username, mb.avatar, MATCH (fq.text, fq.detail) 
          AGAINST (:searchedContent1 IN NATURAL LANGUAGE MODE) as cost FROM forum_questions fq
          JOIN members mb ON mb.usid = fq.user_id
          WHERE MATCH (fq.text, fq.detail) 
          AGAINST (:searchedContent2 IN NATURAL LANGUAGE MODE)
          
          UNION 
          
          SELECT fq.*, mb.usid, mb.username, mb.avatar, MATCH (fa.text) 
          AGAINST (:searchedContent3 IN NATURAL LANGUAGE MODE) as cost FROM forum_questions fq
          JOIN forum_answers fa ON fa.question_id = fq.id 
          JOIN members mb ON mb.usid = fq.user_id
          WHERE MATCH (fa.text) 
          AGAINST (:searchedContent4 IN NATURAL LANGUAGE MODE)) as search
          GROUP BY id
          Order by cost DESC
          Limit 100
          ",
          array(
            'searchedContent1' => $request->get('text'),
            'searchedContent2' => $request->get('text'),
            'searchedContent3' => $request->get('text'),
            'searchedContent4' => $request->get('text'),
          )
        );

        $response = [];
        foreach ($questions as $question) {
            $questionArr = (array) $question;

            $questionArr['user'] = [
                'usid' => $questionArr['usid'],
                'username' => $questionArr['username'],
                'avatar' => $questionArr['avatar']
            ];

            $response[] = new QuestionsView(
                $questionArr
            );
        }

        Log::debug("Request search ended " . $request->get('text') . microtime());

        return response()->json([
                'content' => $response,
                'server_date' => strtotime('now')
            ]
        );
    }


    /**
     * Return all questions that contain the searched field
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function performSearch(Request $request)
    {
        if (is_null($request->get('text'))) {
            return response()->json('Validation error', 400);
        }

        $filterByAudience = '';
        if (!is_null($request->route('audience'))) {

            $response = $this->authService->validateRequestOrigin(
                $request->route('audience')
            );

            $filterByAudience = $request->route('audience');

            if($response !== true){
                return response()->json('No rights to see this content', 403);
            }
        }

        Log::debug("Request search started " . $request->get('text') . microtime());

        $searchedString = $request->get('text');
        $removeChars = ['*'];
        $searchedString = str_replace($removeChars, ' ', $searchedString);

        //append * after each word
        $searchedString = preg_replace('/(\w+){3,}/', '$0*', $searchedString);

        $removeChars = ['+', '-', '>', '<', '(', ')', '~'];
        $searchedString = str_replace($removeChars, ' ', $searchedString);

        $questions = DB::select(" 
          SELECT fq.*, mb.usid, mb.username, mb.avatar, mb.is_mentor, 
            ((MATCH (fq.text) AGAINST (:searchedContent1 IN BOOLEAN MODE) * 1.2) + 
            (MATCH (fq.detail) AGAINST (:searchedContent2 IN BOOLEAN MODE) * 1.1) + 
            (MATCH (fa.text) AGAINST (:searchedContent3 IN BOOLEAN MODE)) * 1.0) as relevance
          FROM forum_questions fq
            JOIN members mb ON mb.usid = fq.user_id
            Left JOIN forum_answers fa ON fa.question_id = fq.id 
          WHERE 
          fq.audience = :audience AND 
          (MATCH (fq.text) 
            AGAINST (:searchedContent4 IN BOOLEAN MODE) OR 
          MATCH (fq.detail) 
            AGAINST (:searchedContent5 IN BOOLEAN MODE) OR 
          MATCH (fa.text) 
            AGAINST (:searchedContent6 IN BOOLEAN MODE))
          Group By id
          ORDER BY relevance DESC
          Limit 100
          ",
            array(
                'searchedContent1' => $searchedString,
                'searchedContent2' => $searchedString,
                'searchedContent3' => $searchedString,
                'audience'         => $filterByAudience,
                'searchedContent4' => $searchedString,
                'searchedContent5' => $searchedString,
                'searchedContent6' => $searchedString,
            )
        );

        $response = [];
        foreach ($questions as $question) {
            $questionArr = (array) $question;

            $questionArr['user'] = [
                'usid' => $questionArr['usid'],
                'username' => $questionArr['username'],
                'avatar' => $questionArr['avatar'],
                'is_mentor' => $questionArr['is_mentor']
            ];

            $questionArr['objTags'] = explode('|', $questionArr['tags']);

            $response[] = new QuestionsView(
                $questionArr
            );
        }

        Log::debug("Request search ended " . $searchedString . microtime());

        return response()->json([
                'content' => $response,
                'server_date' => strtotime('now')
            ]
        );
    }
}