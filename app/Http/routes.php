<?php

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//DB::listen(function($sql) {
//    var_dump($sql->sql);
//});


Route::group([
    'middleware' => ['amazing.authstatus', 'jwt.auth'],
    'namespace' => 'Questions'
    ],
    function () {
        Route::get('/api/questions/{question_id}', ['uses' => 'RestQuestionsController@getQuestion']);
        Route::get('/api/questions/{question_id}/audience/{audience}', ['uses' => 'RestQuestionsController@getQuestion']);

        Route::get('/api/questions', ['uses' => 'RestQuestionsController@getQuestions']);
        Route::get('/api/questions/audience/{audience}', ['uses' => 'RestQuestionsController@getQuestions']);

        Route::get('/api/questions/lessonid/{lesson_id}', ['uses' => 'RestQuestionsController@getQuestionsByLessonId']);
        Route::get('/api/questions/lessonid/{lesson_id}/audience/{audience}', ['uses' => 'RestQuestionsController@getQuestionsByLessonId']);

        Route::get('/api/questions/coursetag/{course_tag}', ['uses' => 'RestQuestionsController@getQuestionsByCourseTag']);
        Route::get('/api/questions/coursetag/{course_tag}/audience/{audience}', ['uses' => 'RestQuestionsController@getQuestionsByCourseTag']);

        Route::post('/api/questions', ['uses' => 'RestQuestionsController@createQuestion']);
        Route::post('/api/questions/audience/{audience}', ['uses' => 'RestQuestionsController@createQuestion']);

        Route::patch('/api/questions/{question_id}', ['uses' => 'RestQuestionsController@editQuestion']);
        Route::patch('/api/questions/{question_id}/audience/{audience}', ['uses' => 'RestQuestionsController@editQuestion']);

        Route::delete('/api/questions/{question_id}', ['uses' => 'RestQuestionsController@deleteQuestion']);
        Route::delete('/api/questions/{question_id}/audience/{audience}', ['uses' => 'RestQuestionsController@deleteQuestion']);
    }
);

Route::group([
    'middleware' => ['amazing.authstatus', 'jwt.auth'],
    'namespace' => 'Answers'
    ],
    function () {
        Route::get('/api/questions/{question_id}/answers', ['uses' => 'RestAnswersController@getAnswers']);
        Route::get('/api/questions/{question_id}/answers/audience/{audience}', ['uses' => 'RestAnswersController@getAnswers']);

        Route::get('/api/questions/{question_id}/answers/{answer_id}', 'RestAnswersController@getAnswer');
        Route::get('/api/questions/{question_id}/answers/{answer_id}/audience/{audience}', 'RestAnswersController@getAnswer');

        Route::patch('/api/questions/{question_id}/answers/{answer_id}', 'RestAnswersController@editAnswer');
        Route::patch('/api/questions/{question_id}/answers/{answer_id}/audience/{audience}', 'RestAnswersController@editAnswer');

        Route::post('/api/questions/{question_id}/answers', 'RestAnswersController@createAnswer');
        Route::post('/api/questions/{question_id}/answers/{answer_id}/vote', 'RestAnswersController@createAnswerVote');
        Route::post('/api/questions/{question_id}/answers/{answer_id}/accept', 'RestAnswersController@createSolution');
    }
);

Route::group([
    'middleware' => ['amazing.authstatus', 'jwt.auth'],
    'namespace' => 'Tags'
    ],
    function () {
        Route::get('/api/tags', 'RestTagsController@getSuggestedTags');
    }
);

Route::group([
    'middleware' => ['amazing.authstatus', 'jwt.auth'],
    'namespace' => 'Search'
    ],
    function () {
        Route::get('/api/search', 'RestSearchController@performSearch');
        Route::get('/api/search/audience/{audience}', 'RestSearchController@performSearch');

        Route::get('/api/search2', 'RestSearchController@performSearch2');
    }
);

Route::group([
    'middleware' => ['amazing.authstatus', 'jwt.auth'],
    'namespace' => 'Profile'
    ],
    function () {
        Route::get('/api/user/{username}/profile', 'RestProfileController@getProfile');
    }
);

Route::any('{all}', function () {
    return response('Not found', '404')
        ->header('Content-Type', 'text');
});