<?php
/**
 * Created by PhpStorm.
 * User: marius
 * Date: 30/08/2016
 * Time: 16:56
 */

namespace Amazing\Model;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'forum_tags';

    public $incrementing = true;

    public function questions()
    {
        return $this->belongsToMany(Question::class, 'forum_questions_tags');
    }

    public function answers()
    {
        return $this->belongsToMany(Question::class, 'forum_answers_tags');
    }
}
