<?php

namespace Amazing\Model;

use Illuminate\Database\Eloquent\Model;

class AnswerVote extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'forum_answers_votes';

    public $incrementing = true;

    public function member()
    {
        return $this->belongsTo(Member::class, 'user_id');
    }

    public function answer()
    {
        return $this->belongsTo(Answer::class, 'answer_id');
    }
}