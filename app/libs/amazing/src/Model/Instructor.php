<?php

namespace Amazing\Model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Instructor extends Model
{
    protected $table = 'course_instructors';

    protected $primaryKey = 'instid';

    protected $guarded = ['instid'];

    protected $dates = ['updated_at'];

    public $timestamps = true;

    /*
     |--------------------------------------------------------------------------
     | RELATIONSHIPS
     |--------------------------------------------------------------------------
     */

    public function member()
    {
        return $this->belongsTo(Member::class, 'usid', 'usid');
    }
}