<?php

namespace Amazing\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Upload extends Model
{
    const TYPE_S3 = 's3';
    const TYPE_JWP = 'jwplatform';

    use SoftDeletes;

    protected $table = 'uploads';

    protected $guarded = ['id'];

    protected $dates = ['created_at', 'updated_at', 'completed_at', 'deleted_at'];

    public $timestamps = true;

    protected $appends = ['url'];

    /*
     |--------------------------------------------------------------------------
     | RELATIONSHIPS
     |--------------------------------------------------------------------------
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sources()
    {
        return $this->hasMany(Upload::class, 'parent_id');
    }

    /*
     |--------------------------------------------------------------------------
     | METHODS
     |--------------------------------------------------------------------------
     */

    /**
     * @return string
     */
    public function type()
    {
        if ($this->bucket == self::TYPE_JWP) {
            return self::TYPE_JWP;
        }

        return self::TYPE_S3;
    }

    /**
     * Mark Video as completed
     */
    public function markComplete()
    {
        $this->completed_at = Carbon::now();
    }

    /**
     * @return null|string
     */
    public function getUrlAttribute()
    {
        if ($this->type() == self::TYPE_S3) {
            return implode('/', [
                env('AWS_S3_ENDPOINT'),
                $this->bucket,
                $this->key
            ]);
        }

        return null;
    }

}