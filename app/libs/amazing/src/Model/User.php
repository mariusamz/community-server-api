<?php

namespace Amazing\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends  Authenticatable
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    public $incrementing = true;

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function question()
    {
        return $this->hasMany(Question::class);
    }
}
