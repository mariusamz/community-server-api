<?php


namespace Amazing\Services;

use Amazing\Services\Auth\AuthService;

class MemberService{

    function __construct(AuthService $auth_service)
    {
        $this->auth_service = $auth_service;
    }


    /**
     * @return 0|1
     */

    public function isMemberMentor(){

        return $this->auth_service->getClaimVal('mentor');
    }
}