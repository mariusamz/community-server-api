<?php

/**
 * Created by PhpStorm.
 * User: marius
 * Date: 09/11/2016
 * Time: 14:10
 */
namespace Amazing\Services;

use Illuminate\Support\Facades\DB;

class QuestionEvents
{

    /**
     * @param int $questionId
     */
    function updateActiveQuestionInfo($questionId){

        DB::statement("Update forum_questions SET latest_activity_date = Now() Where id = :id LIMIT 1", array(
            'id' => $questionId
        ));
    }

}