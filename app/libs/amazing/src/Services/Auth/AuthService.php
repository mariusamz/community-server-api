<?php
/**
 * Created by PhpStorm.
 * User: christianstavro
 * Date: 1/14/16
 * Time: 9:31 AM
 */

namespace Amazing\Services\Auth;


use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

class AuthService
{
    public function getAuthToken()
    {
        
        return JWTAuth::parseToken();
    }

    public function getUserFromAuthToken()
    {
        
        $user = JWTAuth::parseToken()->toUser();
        return $user;
    }

    public function getAuthenticatedUser()
    {
        
        $user = null;
        try {

            if (! $user = JWTAuth::parseToken()->toUser()) {
                return null;
            }
        }
        catch ( JWTException $e) {

            return null;
        }


        // the token is valid and we have found the user via the sub claim
        return $user;
    }

    /**
     * Validate claim of request origin
     * @param string $origin
     * @return bool|null
     */
    public function validateRequestOrigin($origin)
    {
        if(empty($origin)){
            return false;
        }

        try {
            $payload = JWTAuth::parseToken()->getPayload();
            $claimContent = $payload->get('P_ms_abbrs');

            if(!empty($claimContent)) {
                $claimContentArr = explode(',', $claimContent);

                if(!empty($claimContentArr) && in_array($origin, $claimContentArr)) {
                    return true;
                }
            }

        }
        catch ( JWTException $e) {
            return null;
        }

        return false;
    }

    public function getUserFromTokenParam( $token )
    {
        
        $user = JWTAuth::toUser( $token );
        return $user;
    }

    public function getRefreshedAuthToken()
    {
        
        $refreshed_token = JWTAuth::parseToken()->refresh();
        return $refreshed_token;
    }

    public function getRefreshedAuthTokenSalted( $token = null )
    {
        $refreshed_token = $token ? $token : $this->getRefreshedAuthToken();
        $customClaims = $this->getClaimsArray( $refreshed_token );

        $sub = array_get($customClaims, 'sub', '');
        $nbf = array_get($customClaims, 'nbf', '');


        $customClaims['rnd'] = Uuid::generate( 4 )->string;
        $customClaims['jti'] = md5(sprintf('jti.%s.%s', $sub, $nbf))."".$customClaims['rnd'];

        $payload = app('tymon.jwt.payload.factory')->make($customClaims);

        $token = JWTAuth::encode($payload)->get();

        return $token;
    }

    function getMemberId()
    {
        
        try
        {
            return $this->getMemberIdWithException();
        }
        catch( \Exception $e )
        {

        }

        return null;
    }

    function getMemberIdWithException()
    {
        $custom_claims = $this->getClaimsArray();
        if (isset($custom_claims['sub'])) {
            $member_id = $custom_claims['sub'];
            return $member_id;
        }

        return null;
    }

    public function authenticate(Request $request)
    {
        
        $credentials = $request->only('email', 'password');
        return $this->authenticateWithCredentials( $credentials );
    }

    public function authenticateWithCredentials( $credentials )
    {
        
        // grab credentials from the request

        $token = null;

        $auth_info = [];

        try {

            // attempt to verify the credentials and create a token for the user
            if (! $token = $this->jwtAttempt( $credentials ) ) {
                $auth_info['error'] = 'invalid_credentials';
            }
            else
            {
                $member = $this->getUserFromTokenParam( $token );
                $custom_claims = $this->getInitialCustomClaimsFromMember( $member );
                $token = $this->jwtFromUser($member, $custom_claims);

                $auth_info['token'] = $token;
                $auth_info['custom_claims'] = $custom_claims;
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            $auth_info['error'] = 'could_not_create_token';
        }

        return $auth_info;
    }

    public function jwtFromUser( $member, $custom_claims )
    {
        
        return JWTAuth::fromUser($member, $custom_claims);
    }

    public function jwtAttempt( $credentials )
    {
        
        return JWTAuth::attempt($credentials);
    }

    public function getInitialCustomClaimsFromMember( Member $member )
    {
        
        $member_memberships = $member->memberMemberships;

        $username = $member->username ? $member->username : "";
        $custom_claims = [ 'username' => $username ];

        $pro_membership = null;

        $mslids = [];

        foreach( $member_memberships as $member_membership )
        {
            if( $member_membership->mslid == 47 ||  $member_membership->mslid == 56 )
            {
                $pro_membership = $member_membership->mslid;
            }
            else
            {
                $mslids[] = $member_membership->mslid;
            }
        }

        if( $pro_membership != null )
        {
            $custom_claims['AMM_mslid'] = $pro_membership;
        }
        else
        {
            $custom_claims['AMM_mslid'] = 53;
        }

        $mslids_string = implode( ",", $mslids );

        $custom_claims['P_mslids'] = $mslids_string;

        return $custom_claims;
    }

    public function generateTokenForMember( Member $member )
    {
        $custom_claims = $this->getInitialCustomClaimsFromMember( $member );
        $token = JWTAuth::fromUser($member, $custom_claims);
        $auth_info['token'] = $this->getRefreshedAuthTokenSalted( $token );

        return $auth_info;
    }

    public function getClaimsArray( $token = null )
    {
        $payload = null;

        if( $token == null )
        {
            $payload = JWTAuth::parseToken()->getPayload();
        }
        else
        {
            $payload = JWTAuth::setToken( $token )->getPayload( $token );
        }

        return $payload->toArray();
    }

    public function getClaimVal( $key, $token = null )
    {
        $payload = null;

        if( $token == null ) {
            $payload = JWTAuth::parseToken()->getPayload();
        }
        else
        {
            $payload = JWTAuth::setToken( $token )->getPayload();
        }

        if( $payload->offsetExists( $key ) === false )
        {
            return null;
        }

        return $payload[$key];
    }


}