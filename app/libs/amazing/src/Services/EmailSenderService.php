<?php

namespace Amazing\Services;


class EmailSenderService{

    const FROM_EMAIL = 'no-reply@amazingsellingmachine.com';
    const REPLY_EMAIL = 'no-reply@amazingsellingmachine.com';
    const PATH_TEMPLATE = 'resources/views/emails/';


    /**
     * @param $senderList array Member
     * @param $templatePath string
     * @param $subject string
     * @param $variables array
     *
     * @return bool
     */
    public function sendEmails($senderList, $templatePath, $subject, $variables){

        //get template content to string
        $template = file_get_contents($templatePath);

        $template = str_replace("[Question-Name]", $variables['questionName'], $template);
        $template = str_replace("[QUESTION-URL]", $variables['questionURL'], $template);

        if($template !== false) {
            $headers = 'From: '. self::FROM_EMAIL . "\r\n" .
                'Reply-To: ' . self::REPLY_EMAIL . "\r\n" ;

            foreach ($senderList as $member) {
                mail($member->email, $subject, $template, $headers);
            }

            return true;
        }

        return false;
    }

    /**
     * @param $senderList array Member
     * @param $typeOfEmail integer
     * @param $variables array
     */
    public function parseRequest($senderList, $typeOfEmail, $variables){

        //notify owner answers
        if($typeOfEmail === 1){
            $this->sendEmails($senderList,
                base_path() . '/' . self::PATH_TEMPLATE . 'notifyOwnerAnswer.txt',
                'There is a new response to your post',
                $variables);
        }

        //notify owner question
        elseif($typeOfEmail === 2){
            $this->sendEmails($senderList,
                base_path() . '/' . self::PATH_TEMPLATE . 'notifyOwnerQuestion.txt',
                'There is a new response to your post',
                $variables);
        }
    }
}