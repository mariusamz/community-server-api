<?php

namespace Amazing\Services;

use Amazing\Model\Answer;
use Amazing\Model\Question;

class NotificationService{

    private $emailSenderService;

    /**
     * NotificationService constructor.
     * @param EmailSenderService $emailSenderService
     */
    function __construct(EmailSenderService $emailSenderService)
    {
        $this->emailSenderService = $emailSenderService;
    }

    public function answerAddEvent(Answer $addedAnswer, Question $question){

        $answers = Answer::with('member')
            ->where('question_id', $question->id)
            ->get();

        $membersToNotify = [];

        /** @var Answer $answer */
        foreach($answers as $answer) {
            $membersToNotify[$answer->member->usid] = $answer->member;
        }

        //remove the owner of the added Answer from the list of members we want to notify
        if(isset($membersToNotify[$addedAnswer->member->usid])) {
            unset($membersToNotify[$addedAnswer->member->usid]);
        }

        //remove the owner of the Question owning the Answer that was just added
        //from the list of members we want to notify
        if(isset($membersToNotify[$question->member->usid])) {
            unset($membersToNotify[$question->member->usid]);
        }

        //send notification to the owner(s) of the answers in the Question thread
        if(count($membersToNotify)) {
            $this->emailSenderService->parseRequest(
                $membersToNotify,
                1,
                ['questionName' => $question->name, 'questionURL' => env('DOMAIN_URL') . '/questions/' . $question->id]
            );
        }

        //send notification to the owner of the Question
        if($question->member->usid !== $addedAnswer->member->usid) {
            $this->emailSenderService->parseRequest(
                [$question->member],
                2,
                ['questionName' => $question->name, 'questionURL' => env('DOMAIN_URL') . '/questions/' . $question->id]
            );
        }

    }
}