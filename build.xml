<?xml version="1.0" encoding="UTF-8"?>

<project name="Amazing" default="help">
    <property name="build.dir" value="${project.basedir}/build" />
    <property name="bin.dir" value="${project.basedir}/bin" />
    <property name="vendor.dir" value="${project.basedir}/vendor" />
    <property name="vendor.bin" value="${vendor.dir}/bin" />
    <property name="tarball.name" value="amazing-build-${env.BUILD_NUMBER}-${env.GIT_COMMIT}.tgz" />
    <property name="report.dir" value="${build.dir}/report"/>
    <property name="threads" value="4"/>
    <property name="failUnitTest" value="true" />

    <target name="composer" depends="composer-selfupdate, composer-update"/>

    <target name="refresh-db">
        <echo message="Reseting the database" />
        <exec executable="php" logoutput="true" passthru="true">
            <arg value="artisan" />
            <arg value="migrate:refresh" />
        </exec>
    </target>

    <target name="migrate-update">
        <echo message="Update with migration changes" />
        <exec executable="php" logoutput="true" passthru="true">
            <arg value="artisan" />
            <arg value="migrate" />
        </exec>
    </target>

    <target name="composer-selfupdate">
        <echo msg="Updating composer..."/>
        <exec executable="composer" logoutput="true" passthru="true">
            <arg value="self-update" />
            <arg value="--ansi" />
        </exec>
        <echo msg="Composer config used:"/>
        <exec executable="${project.basedir}/composer.phar" logoutput="true" passthru="true">
            <arg value="config" />
            <arg value="--list" />
            <arg value="--ansi" />
        </exec>
    </target>

    <target name="composer-install">
        <echo message="Installing dependencies..." />
        <exec executable="composer" logoutput="true" passthru="true">
            <arg value="install" />
            <arg value="--no-progress" />
            <arg value="--verbose" />
            <arg value="--ansi" />
        </exec>
    </target>

    <target name="composer-update">
        <echo message="Updating dependencies..." />
        <exec executable="composer" logoutput="true" passthru="true">
            <arg value="update" />
            <arg value="--no-progress" />
            <arg value="--verbose" />
            <arg value="--ansi" />
        </exec>
    </target>

    <target name="composer-update-no-dev">
        <echo message="Updating dependencies..." />
        <exec executable="composer" logoutput="true" passthru="true">
            <arg value="update" />
            <arg value="--no-dev" />
            <arg value="--no-progress" />
            <arg value="--verbose" />
            <arg value="--ansi" />
        </exec>
    </target>

    <target name="lint">
        <echo message="Linting php files..." />
        <exec executable="${vendor.bin}/parallel-lint" logoutput="true" passthru="true" checkreturn="true">
            <arg line="--exclude" />
            <arg path="${project.basedir}/vendor/" />
            <arg path="${project.basedir}" />
        </exec>
    </target>

    <target name="phpcs">
        <echo message="Running phpcs..." />
        <exec executable="${vendor.bin}/phpcs" logoutput="true">
            <arg value="--version" />
        </exec>
        <exec executable="${vendor.bin}/phpcs" logoutput="true" passthru="true">
            <arg value="--report=checkstyle" />
            <arg value="--report-file=${build.dir}/logs/checkstyle.xml" />
            <arg value="--standard=${project.basedir}/phpcs.xml" />
            <arg value="--extensions=php" />
            <arg value="--ignore=autoload.php, */vendor/*, */old_src/*, */data/*" />
            <arg value="-p" />
            <arg path="${project.basedir}" />
        </exec>
    </target>

    <target name="phpmd"
            description="Perform project mess detection using PHPMD and log result in XML format. Intended for usage within a continuous integration environment."
    >
        <echo message="Running phpmd..." />
        <exec executable="${vendor.bin}/phpmd" logoutput="true" passthru="true">
            <arg path="${project.basedir}/module" />
            <arg value="xml" />
            <arg path="${project.basedir}/phpmd.xml" />
            <arg value="--reportfile" />
            <arg path="${project.basedir}/build/logs/pmd.xml" />
            <arg value="--exclude" />
            <arg value="*/vendor/*" />
        </exec>
    </target>

    <target name="pdepend"
            description="Calculate software metrics using PHP_Depend and log result in XML format. Intended for usage within a continuous integration environment.">
        <echo message="Running pdepend..." />
        <exec executable="${vendor.bin}/pdepend" logoutput="true" passthru="true">
            <arg value="--jdepend-xml=${build.dir}/logs/jdepend.xml" />
            <arg value="--jdepend-chart=${build.dir}/logs/dependencies.svg" />
            <arg value="--overview-pyramid=${build.dir}/logs/overview-pyramid.svg" />
            <arg path="${project.basedir}/module" />
        </exec>
    </target>

    <target name="phpdoc" description="Generate API documentation using PHPDocumentor">
        <exec executable="${vendor.bin}/phpdoc" logoutput="true" passthru="true">
            <arg value="-d" />
            <arg path="${project.basedir}/module" />
            <arg value="-t" />
            <arg path="${build.dir}/doc" />
        </exec>
    </target>

    <target name="test-core-domain">
        <exec executable="${vendor.bin}/phpunit" logoutput="true" passthru="true">
            <arg value="-v"/>

            <arg value="--log-junit" />
            <arg path="${build.dir}/logs/unitreport-core-domain.xml" />

            <arg value="--coverage-html" />
            <arg path="${build.dir}/logs/coverage/core-domain" />

            <arg value="--coverage-php" />
            <arg path="${build.dir}/logs/coverage/cov/coverage-core-domain.cov" />

            <arg value="--configuration"/>
            <arg path="${project.basedir}/src/core-domain/test/configuration.xml"/>
        </exec>
    </target>


    <target name="phpunit"
            description="Run unit tests with PHPUnit">

        <exec executable="${vendor.bin}/phpunit" logoutput="true" passthru="true" checkreturn="${failUnitTest}">
            <arg value="-v"/>

            <arg value="--log-junit" />
            <arg path="${build.dir}/logs/unitreport.xml" />

            <arg value="--coverage-html" />
            <arg path="${build.dir}/logs/coverage" />

            <arg value="--coverage-clover" />
            <arg path="${build.dir}/logs/coverage/coverage.xml" />

            <arg value="--configuration"/>
            <arg path="${project.basedir}/phpunit.xml"/>

            <arg value="--testsuite=E2E"/>
        </exec>
    </target>

    <target name="test-no-coverage">
        <exec executable="${bin.dir}/virtavo" logoutput="true" passthru="true" checkreturn="true">
            <arg value="virtavo:test-fixture"/>
            <arg value="--config=config/config-test.ini"/>
            <arg value="--fixture-name=E2E"/>
            <arg value="--no-prompts"/>
            <arg value="--ansi"/>
        </exec>
        <exec executable="${vendor.bin}/phpunit" logoutput="true" passthru="true" checkreturn="${failUnitTest}">
            <arg value="-v"/>
            <arg value="--configuration"/>
            <arg path="${project.basedir}/phpunit.xml"/>
            <arg value="--testsuite=E2E"/>
        </exec>
    </target>

    <target name="tarball">
        <echo message="Creating tarball..." />

        <exec executable="tar" logoutput="true" checkreturn="true">
            <arg value="-czf"/>
            <arg value="${build.dir}/${tarball.name}"/>
            <arg line="--exclude=${build.dir}/"/>
            <arg line="-C ${project.basedir}/"/>
            <arg path="*" />
        </exec>
    </target>

    <target name="cleanup-after">
        <echo message="Turn off development mode" />
        <exec command="php public/index.php development disable" />

        <echo message="Uninstall dev-only packages" />
        <phingcall target="composer-update-no-dev" />

        <echo message="Remove test config" />
        <delete file="${project.basedir}/config/config-test.ini"/>
        <delete file="${project.basedir}/config/bootstrap_config.php" />
        <copy
                file="${project.basedir}/config/bootstrap_config.php.dist"
                tofile="${project.basedir}/config/bootstrap_config.php"
        />

        <echo message="Clean up caches and logs" />
        <exec command="rm -rvf ${project.basedir}/data/cache/*" checkreturn="true" />
        <exec command="rm -rvf ${project.basedir}/data/orm_cache/*" checkreturn="true" />
        <exec command="rm -rvf ${project.basedir}/data/log/*" checkreturn="true" />
        <exec command="rm -rvf ${project.basedir}/old_src" checkreturn="true" />
        <exec command="rm -rvf ${project.basedir}/salt" checkreturn="true" />

        <echo message="Record and inject version" />
        <exec command="touch ${project.basedir}/public/version.txt" />
        <echo
                file="${project.basedir}/public/version.txt"
                message="build: ${env.BUILD_NUMBER} commit: ${env.GIT_COMMIT}"
        />
    </target>

    <target name="build" depends="composer, lint, phpunit, phpcs, phpmd, pdepend, cleanup-after, tarball" />

    <target name="quickbuild" depends="composer, lint, test-no-coverage, phpcs, phpmd, pdepend, cleanup-after, tarball" />

    <target name="build-doc" depends="composer, phpdoc, cleanup-after" />

    <target name="build-checkstyle" depends="composer, phpcs, cleanup-after" />

    <target name="build-unittest" depends="composer, phpunit, cleanup-after" />

</project>


